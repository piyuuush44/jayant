import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProfileModel} from './models/Profile.model';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isAuthenticated = !!localStorage.getItem('authentication');
  currentUser: ProfileModel = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
  user: Subscription;

  constructor(private service: AppService) {
  }

  ngOnInit(): void {
    this.currentUser = this.service.user.value;
  }

  onLogout() {
    localStorage.clear();
    window.location.href = '/home';
  }
}
