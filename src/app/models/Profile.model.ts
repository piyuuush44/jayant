export class ProfileModel {
  constructor(
    public id?: string,
    public name?: string,
    public authentication?: string,
    public email?: string,
    public user_type?: string
  ) {
  }
}
