import {ProfileModel} from './Profile.model';

export class JobsModel {
  constructor(
    public id?: number,
    public position?: string,
    public status?: string,
    public description?: string,
    public user_id?: number,
    public jobrequest?: [{
      id?: number,
      status?: string,
      user: ProfileModel[]
    }]
  ) {
  }
}
