import {Component, OnDestroy, OnInit} from '@angular/core';
import {JobsModel} from '../models/jobs.model';
import {ProfileModel} from '../models/Profile.model';
import {Subscription} from 'rxjs';
import {AppService} from '../app.service';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  url = '';
  user: ProfileModel;
  subscription: Subscription;
  jobs: JobsModel[] = [];
  appliedUsers: ProfileModel[];
  viewUserCheck = false;
  httpOptions: { headers; observe; } = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      authentication: localStorage.getItem('authentication')
    }),
    observe: 'response'
  };

  constructor(private service: AppService, private http: HttpClient) {
  }

  ngOnInit() {
    this.user = this.service.user.value;
    if (this.user.user_type === 'candidate') {
      this.url = 'http://localhost/hireme/public/api/fetch_jobs';
    } else {
      this.url = 'http://localhost/hireme/public/api/get_user_jobs';
    }
    this.http.get(this.url, this.httpOptions).subscribe((value: HttpResponse<any>) => {
      if (value.status === 200) {
        this.jobs = value.body;
      }
    });

  }

  // on click of view applicant
  viewApplicants(i: number) {
    this.appliedUsers = [];
    this.jobs[i].jobrequest.forEach(job => {
      job.user.forEach(u => {
        this.appliedUsers.push(u);
      });
    });
    this.viewUserCheck = true;
  }

  // on click of job apply button
  jobApply(i: number) {
    this.http.post('http://localhost/hireme/public/api/apply_for_job', {
      user_id: this.user.id,
      job_id: this.jobs[i].id
    }, this.httpOptions).subscribe((value: HttpResponse<any>) => {
      if (value.status === 200) {
        alert('applied successfully');
      }
    });
  }

  ngOnDestroy(): void {
    this.viewUserCheck = false;
  }

}
