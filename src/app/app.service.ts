import {ProfileModel} from './models/Profile.model';
import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class AppService {
  user = new BehaviorSubject<ProfileModel>(JSON.parse(localStorage.getItem('user')));
}
