import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {ProfileModel} from '../models/Profile.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private service: AppService) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.http.post('http://localhost/hireme/public/api/login', form.value, {observe: 'response'}).subscribe(
      (result: HttpResponse<ProfileModel>) => {
        if (result.status === 200) {
          this.service.user.next(result.body);
          localStorage.setItem('user', JSON.stringify(result.body));
          localStorage.setItem('authentication', JSON.stringify(result.body.authentication));
          this.router.navigate(['./profile']);
        } else {
          alert('User not found');
        }
      }
    );
  }
}
