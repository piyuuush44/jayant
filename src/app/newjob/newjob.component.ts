import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-newjob',
  templateUrl: './newjob.component.html',
  styleUrls: ['./newjob.component.css']
})
export class NewjobComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    const httpOptions: { headers; observe; } = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        authentication: localStorage.getItem('authentication')
      }),
      observe: 'response'
    };

    this.http.post('http://localhost/hireme/public/api/new_job', form.value, httpOptions).subscribe(
      (result: HttpResponse<any>) => {
        if (result.status === 200) {
          alert('Job posted successfully');
          this.router.navigate(['./profile']);
        } else {
          alert('Job already found');
        }
      }
    );
  }

}
