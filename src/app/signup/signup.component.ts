import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {AppService} from '../app.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private service: AppService) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.http.post('http://localhost/hireme/public/api/register', form.value, {observe: 'response'}).subscribe(
      (result: HttpResponse<any>) => {
        if (result.status === 200) {
          this.service.user.next(result.body);
          localStorage.setItem('user', JSON.stringify(result.body));
          localStorage.setItem('authentication', JSON.stringify(result.body.authentication));
          this.router.navigate(['./profile']);
        } else {
          alert('User already found');
        }
      }
    );
  }
}
